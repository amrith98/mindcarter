var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var QnSchema=new Schema({
	num:{
		type:Number,
		required:true
	},
	quest:{
		type:String,
		required:true
	},
	opts:{
	1:{
		type:String,
		required:true
	},
	2:{
		type:String,
		required:true
	},
	3:{
		type:String,
		required:true
	},
	4:{
		type:String,
		required:true
	},
	5:{
		type:String,
		required:true
	}
}
});
module.exports = mongoose.model('PQ_Questions',QnSchema);

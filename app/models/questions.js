var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var QnSchema=new Schema({
	num:{
		type:Number,
		required:true
	},
	quest:{
		type:String,
		required:true
	},
	opts:{a:{
		type:String,
		required:true
	},
	b:{
		type:String,
		required:true
	},
	c:{
		type:String,
		required:true
	},
	d:{
		type:String,
		required:true
	}}

});
module.exports = mongoose.model('Questions',QnSchema);

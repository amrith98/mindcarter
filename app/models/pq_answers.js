var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var AnsSchema=new Schema({
	num:{
		type:Number,
		required:true
	},
	points:{a:{
		type:Number,
		required:true
	},
	b:{
		type:Number,
		required:true
	},
	c:{
		type:Number,
		required:true
	},
	d:{
		type:Number,
		required:true
	},
	e:{
		type:Number,
		required:true
	}}

});
module.exports = mongoose.model('PQ_Answers',AnsSchema);

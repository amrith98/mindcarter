var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var passport = require('passport');
var config = require('./config/database');
var User = require('./app/models/user');
var EQ_Question = require('./app/models/questions');
var PQ_Question = require('./app/models/pq_questions');
var Answers = require('./app/models/answers');
var PQ_Answers= require('./app/models/answers');
var port = 1983;
var jwt = require('jwt-simple');
var nodemailer = require('nodemailer');
var apiRoutes = express.Router();
app.use(express.static('public'));
mongoose.connect(config.database);
require('./config/passport')(passport);
//get params
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
//console log
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.set('views', __dirname + '/public');
app.use(morgan('dev'));

//use passport
app.use(passport.initialize());
var currentUser = null;
//mail config
var smtpTransport = nodemailer.createTransport("SMTP", {
    service: "Gmail",
    auth: {
        user: "systemadmin@mindcarter.com",
        pass: "mindcartertesting"
    }
});
//route /
app.get('/', function(req, res) {
    res.send();
});
app.post('/signup', function(req, res) {
    if (!req.body.name || !req.body.password) {
        res.json({ success: false, msg: 'Please pass name and password.' });
    } else {
        var newUser = new User({
            name: req.body.name,
            sex: req.body.sex,
            age: req.body.age,
            address: req.body.addr,
            contact: req.body.phone,
            email: req.body.email,
            password: req.body.password,
            status:0,
            eq_result:{},
            pq_result:{},
            date:new Date()
        });
        // save the user
        newUser.save(function(err) {
            if (err) {
                console.log(err);
                return res.json({ success: false, msg: 'Username already exists.' });
            }
            else{
            res.json({ success: true, msg: 'Successful created new user.' });}
        });
    }
});
app.post('/login', function(req, res) {
    User.findOne({
        contact: req.body.phone
    }, function(err, user) {
        if (err) throw err;
        if (!user) {
            res.send({ success: false, msg: 'Authentication failed. User not found.' });
        } else {
            user.comparePassword(req.body.password, function(err, isMatch) {
                if (isMatch && !err) {
                    currentUser = user;
                    var token = jwt.encode(user, config.secret);
                    res.json({ redirect: "/#/", success: true, token: 'JWT ' + token,status:user.status });
                } else {
                    res.send({ success: false, msg: 'Authentication failed. Wrong password.' });
                }
            });
        }
    });
});
app.post('/all_pq_questions',function(req,res){
    PQ_Question.find({}, function(err, questions) {
        if (err) console.log(err);
        else
            res.json(questions);
    });
});
app.post('/question', function(req, res) {
    var qnum = req.body.qnum;
    EQ_Question.findOne({ 'num': qnum }, function(err, qn) {
        if (err)
            return handleError(err);
        else {
            res.json(qn);
        }
    });
});
app.post('/all_eq_questions', function(req, res) {
    EQ_Question.find({}, function(err, questions) {
        if (err) console.log(err);
        else
            res.json(questions);
    });
});
app.post('/answer', function(req, res) {
    var anum = req.body.anum;
    var options = req.body.options;
    console.log(options);
    Answers.findOne({ 'num': anum }, function(err, ans) {
        if (err)
            return handleError(err);
        else {
            res.json(ans.points[options]);
        }
    });
});
app.post('/eq_calculate', function(req, res) {
    var scores = [];
    var final, total = 0;
    var answers = req.body.answer;
    var answers_array = answers.split(",");
    console.log(answers_array);
    console.log(answers_array.length)
    var sensitivity = [2, 8, 16, 17, 22];
    var maturity = [4, 6, 9, 11, 12, 18, 21];
    var competency = [1, 3, 5, 7, 10, 13, 14, 15, 19, 20];
    var sub_scores = {
        "sensitivity": 0,
        "maturity": 0,
        "competency": 0,
        "total_eq": 0
    };
    function sum(array) {
        var sum = 0;
        for (var j = 0; j < array.length; j++) {
            sum += parseInt(array[j]);
        }
        return sum;
    };

    function sub_sum(array, sub) {
        var sum = 0;
        for (var j = 0; j < sub.length; j++) {
            sum += parseInt(array[sub[j] - 1]);
        }
        return sum;
    };
    var i = 0;
    var flag = 0;
    var finalAnswers = [];
    Answers.find({}, function(err, answers) {
        if (err) console.log(err);
        else {
            for (i = 0; i < answers_array.length; i++) {
                var option = answers_array[i];
                //console.log('ans '+answers[i].points[option]+' opt'+option+' count'+(i+1));
                scores[i] = answers[i].points[option];
            }
            sub_scores["sensitivity"] = sub_sum(scores, sensitivity);
            sub_scores["maturity"] = sub_sum(scores, maturity);
            sub_scores["competency"] = sub_sum(scores, competency);
            sub_scores["total_eq"] = sum(scores);
            console.log('Total Score: ' + sum(scores));
            console.log('sensitivity Score: ' + sub_sum(scores, sensitivity));
            console.log('maturity Score: ' + sub_sum(scores, maturity));
            console.log('competency Score: ' + sub_sum(scores, competency));
            console.log('currentUser ' + currentUser);
            User.update({contact:currentUser.contact},{$set:{status:0,eq_result:sub_scores,date:new Date()}},function(err,doc){
                    if(err) console.log(err);
                    else
                    {
                        console.log(doc);
                    }
            });
            var mailOptions = {
                from: 'systemadmin@mindcarter.com',
                to: 'systemadmin@mindcarter.com',
                subject: 'EQ Test Result of '+currentUser.name,
                html: '<h1>EQ Test Results</h1>'+'<br><h3> User Details</h3><br><p>Name : ' + currentUser.name + '</p><p>Sex :' + currentUser.sex + '</p><p>Age :' + currentUser.age + '</p><p>Address :' + currentUser.address + '</p><p>Contact :' + currentUser.contact + '</p><p>Email :' + currentUser.email + '</p><br><h3>Test Scores</h3><br><p>Sensitivity :' + sub_scores.sensitivity +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Range of Score is 25-100</b>'+ '</p><p>Maturity : ' + sub_scores.maturity+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Range of Score is 35-140</b>'+'</p><p>Competency : ' + sub_scores.competency +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Range of Score is 100-200</b>'+ '</p><p>Total EQ : ' + sub_scores.total_eq +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Range of Score is 110-440</b>'
            };
            smtpTransport.sendMail(mailOptions, function(error, response) {
                if (error) {
                    console.log(error);
                } else {
                    console.log(response);
                    res.send();
                }
            });
            res.send();
        }
    });
});
//Personality Test
app.post('/pq_calculate', function(req, res) {
    var result_array=(req.body.answer).split(",");
    var r=[];
    for (var j = 0; j < result_array.length; j++) {
            r[j]=parseInt(result_array[j]);
    }
    //var extroversion=[1,6,11,16,21,26,31,36,41,46];
    var ext=20+r[0]-r[5]+r[10]-r[15]+r[20]-r[25]+r[30]-r[35]+r[40]-r[45];
    //var agreeableness=[2,7,12,17,22,27,32,37,42,47];
    var agree=14-r[1]+r[6]-r[11]+r[16]-r[21]+r[26]-r[31]+r[36]+r[41]+r[46];
    //var conscientiousness=[3,8,13,18,23,28,33,38,43,48];
    var conscience=14+r[2]-r[7]+r[12]-r[17]+r[22]-r[27]+r[32]-r[37]+r[42]+r[47];
    //var neuroticism=[4,9,14,19,24,29,34,39,44,49];
    var neuro=38-r[3]+r[8]-r[13]+r[18]-r[23]-r[28]-r[33]-r[38]-r[43]-r[48];
    //var openness=[5,10,15,20,25,30,35,40,45,50];
    var open=8+r[4]-r[9]+r[14]-r[19]+r[24]-r[29]+r[34]+r[39]+r[44]+r[49];

    var sub_scores = {
        "extroversion": ext,
        "agreeableness": agree,
        "conscientiousness": conscience,
        "neuroticism": neuro,
        "openness": open
    };
    console.log(sub_scores);
    User.update({contact:currentUser.contact},{$set:{status:0,pq_result:sub_scores,date:new Date()}},function(err,doc){
            if(err) console.log(err);
            else
            {
                console.log(doc);
            }
    });
    var mailOptions = {
        from: 'systemadmin@mindcarter.com',
        to: 'systemadmin@mindcarter.com',
        subject: 'Personality Test Result of '+currentUser.name,
        html: '<h1>Personality Test Results</h1>'+'<br><h3> User Details</h3><br><p>Name : ' + currentUser.name + '</p><p>Sex :' + currentUser.sex + '</p><p>Age :' + currentUser.age + '</p><p>Address :' + currentUser.address + '</p><p>Contact :' + currentUser.contact + '</p><p>Email :' + currentUser.email + '</p><br><h3>Test Scores</h3><br><p>Extroversion: ' + sub_scores.extroversion +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Range of Score is 0-40</b>'+ '</p><p>Agreeableness: ' + sub_scores.agreeableness+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Range of Score is 0-40</b>'+'</p><p>Conscientiousness : ' + sub_scores.conscientiousness +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Range of Score is 0-40</b>'+'</p><p>Neuroticism : ' + sub_scores.neuroticism +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Range of Score is 0-40</b>'+'</p><p>Openness to Experience: ' + sub_scores.openness +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Range of Score is 0-40</b>'
    };
    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log(response);
            res.send();
        }
    });
    res.send();    
});
app.use('/api', apiRoutes);
app.listen(port);
console.log('Server up @ port ' + port);

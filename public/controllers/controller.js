var app = angular.module('mindcarter', ['ngRoute', 'ngMaterial', 'ngAria', 'ngMessages', 'ngMdIcons', 'angular-loading-bar']);
app.config(function($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: 'login_page.html',
        controller: 'mainctrl'
    }).when('/start', {
        templateUrl: 'start_page.html',
        controller: 'mainctrl'
    }).when('/register', {
        templateUrl: 'reg.html',
        controller: 'mainctrl'
    }).when('/eq_quiz', {
        templateUrl: 'QA.html',
        controller: 'EQ_quizctrl'
    }).when('/noaccess', {
        templateUrl: 'noaccess.html',
        controller: 'mainctrl'
    }).when('/eq_end', {
        templateUrl: 'thanks.html',
        controller: 'EQ_quizctrl'
    }).when('/pq_end', {
        templateUrl: 'thanks.html',
        controller: 'PQ_quizctrl'
    }).when('/pq_quiz', {
        templateUrl: 'QA_personality.html',
        controller: 'PQ_quizctrl'
    }).otherwise({ redirectTo: '/' });
});
app.factory('Auth', function($rootScope, $window, $http, $location) {
    return {
        login: function(user, successHandler, errorHandler) {
            $http.post('/login', user).success(function(user) {
                if (user.success == false) {
                    alert('Wrong Credentials');
                    user = null;
                } else {
                    $rootScope.user = user;
                    if (user == null) {
                        $window.sessionStorage.removeItem('user');
                    } else {
                        $window.sessionStorage.setItem('user', angular.toJson($rootScope.user.token));
                    }
                    successHandler(user);
                }
            }).error(errorHandler);
        },
        getLoggedInUser: function() {
            if ($rootScope.user === undefined || $rootScope.user == null) {
                var userStr = $window.sessionStorage.getItem('user');
                if (userStr) {
                    $rootScope.user = angular.fromJson(userStr);
                }
            }
            return $rootScope.user;
        },
        isLoggedIn: function() {
            return this.getLoggedInUser() != null;
        },
        logout: function() {
            $rootScope.user = null;
            $window.sessionStorage.removeItem('user');
        }
    }
});
app.run(['$window', '$rootScope', '$location', 'Auth', function($window, $rootScope, $location, Auth) {
    $rootScope.$on("$routeChangeStart", function(event) {
        console.log('authchange');
        console.log(Auth.isLoggedIn());
        if (!Auth.isLoggedIn() && ($location.path() != '/register')) {
            $location.path('/');
        }
    });
}]);
app.controller('mainctrl', function($rootScope, $scope, Auth, $location, $http) {
    $scope.user = {};
    $scope.login = function() {
        Auth.login($scope.user, function() {
            if ($rootScope.user.status == 1) {
                $location.path('/noaccess');
            } else {
                $location.path('/start');
            }
        }, function(e) {
            console.log('login error + ' + e)
        });
    };
    $scope.signup = function() {
        if ($scope.newUser.phone == '' || $scope.newUser.name == '' || $scope.newUser.age == '' || $scope.newUser.sex == '' || $scope.newUser.password == '' || $scope.newUser.email == '' || $scope.newUser.addr == '') {
            alert('Please Fill in All the Fields');
        } else {
            $http.post('/signup', $scope.newUser).success(function(req, res) {
                if (req.success == false) {
                    alert('SignUp Failed, Please Try Again');
                } else {
                    alert('Successfully Registered. Please Login');
                    $location.path('/');
                }
            });
        }
    };
    $scope.startEQ = function() {
        $location.path('/eq_quiz');
    };
    $scope.startPQ = function() {
        $location.path('/pq_quiz');
    };
    $scope.signOut=function(){
        Auth.logout();
        $location.path('/');
    };
    // refresh();
});
app.controller('EQ_quizctrl', function($scope, Auth, $location, $http) {
    $scope.questions = {};
    $scope.answers=[];
    for(var i=0;i<22;i++)
    {
          $scope.answers[i]=0;
    }
    $scope.i =0;
    $scope.selectedAnswer = 'a';
    $http.post('/all_eq_questions', {}).success(function(req, res) {
        $scope.questions = req;
        $scope.currentQuestion = req[$scope.i].quest;
        $scope.currentOptions = req[$scope.i].opts;
        $scope.next = function() {
                $scope.i=$scope.i+1;
                if($scope.i>21)
                {
                    $scope.answers[21]=$scope.selectedAnswer;
                    $scope.endQuiz();
                }
                else
                { 
                    if($scope.answers[$scope.i]!=0)
                    {
                        $scope.selectedAnswer=$scope.answers[$scope.i];
                    }
                    else
                    {                    
                        $scope.answers[$scope.i-1] = $scope.selectedAnswer;
                    }
                    $scope.currentQuestion = req[$scope.i].quest;
                    $scope.currentOptions = req[$scope.i].opts;
                }
                console.log($scope.i+' '+$scope.answers);

            };
           $scope.prev=function(){
                    if ($scope.i < 0) {
                        $scope.i = 0;
                    } else {
                        $scope.answers[$scope.i]=$scope.selectedAnswer;
                        $scope.i = $scope.i - 1;
                        $scope.selectedAnswer=$scope.answers[$scope.i];
                        $scope.currentQuestion = req[$scope.i].quest;
                        $scope.currentOptions = req[$scope.i].opts;
                    }
                    console.log('prev '+$scope.i+' '+$scope.answers);
        };
        $scope.endQuiz = function() {
            $location.path('/eq_end');
            var strfy = ($scope.answers).join(",");
            console.log(strfy);
            var ans = {
                "answer": strfy
            };
            $http.post('/eq_calculate', ans).success(function(req, res) {
                console.log('calculating');
                console.log(req);
                Auth.logout();
                console.log(Auth.isLoggedIn());
            });
        }
    });
}); 
app.controller('PQ_quizctrl',function($scope,Auth,$location,$http){
        $scope.questions = {};
        $scope.answers = [];
        for(var i=0;i<50;i++)
        {
          $scope.answers[i]=0;
        }
        $scope.i = 0;
        $scope.selectedAnswer = '1';
        $http.post('/all_pq_questions', {}).success(function(req, res) {
                $scope.questions = req;
                $scope.currentQuestion = req[$scope.i].quest;
                $scope.currentOptions = req[$scope.i].opts;
                $scope.next = function() {
                    $scope.i=$scope.i+1;
                    if($scope.i>49)
                    {
                        $scope.answers[49]=$scope.selectedAnswer;
                        $scope.endQuiz();
                    }
                    else
                    { 
                        if($scope.answers[$scope.i]!=0)
                        {   
                            $scope.answers[$scope.i-1] = $scope.selectedAnswer;
                            $scope.selectedAnswer=$scope.answers[$scope.i];
                        }
                        else
                        {                    
                            $scope.answers[$scope.i - 1] = $scope.selectedAnswer;
                        }
                        $scope.currentQuestion = req[$scope.i].quest;
                        $scope.currentOptions = req[$scope.i].opts;
                    }
                    console.log('nex '+$scope.i+' '+$scope.answers);
                };
                $scope.prev=function(){
                        if ($scope.i <= 0) {
                            $scope.i = 0;
                        } else {
                            $scope.answers[$scope.i]=$scope.selectedAnswer;
                            $scope.i = $scope.i - 1;
                            $scope.selectedAnswer=$scope.answers[$scope.i];
                            $scope.currentQuestion = req[$scope.i].quest;
                            $scope.currentOptions = req[$scope.i].opts;
                        }
                        console.log('prev '+$scope.i+' '+$scope.answers);

                };
                $scope.endQuiz = function() {
                    $location.path('/pq_end');
                    var strfy = ($scope.answers).join(",");
                    var ans = {
                        "answer": strfy
                    };
                    $http.post('/pq_calculate', ans).success(function(req, res) {
                        console.log('calculating');
                        console.log(req);
                        Auth.logout();
                    });
                }
        });



});